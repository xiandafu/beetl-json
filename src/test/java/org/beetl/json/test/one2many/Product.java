package org.beetl.json.test.one2many;

public class Product {
	String code ;
	int total ;
	public Product(){
		
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	
	
}
